#!.virtualenv/bin/python
# -*- coding: utf-8 -*-
'''
    Simple run script for stuff_management.

    Usage:

    ./run.sh
        (starts the development internal flask server.  FOR DEVELOPMENT ONLY!)

    ./run.sh waitress
        (starts the server running with the waitress production server)
'''
from __future__ import print_function

# Configuration Options:

__HOST__ = u'0.0.0.0'
__PORT__ = 5000
__THREADS__ = 8 # (for waitress, only)

# Initialise unicode:

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

# Load the app:

from stuff_management import app

################################################################################
# different deployment options:

def waitress(*args):
    ''' Production server (pure python) using waitress '''
    from waitress import serve
    serve(app, host=__HOST__, port=__PORT__, threads=__THREADS__)

def dev(*args):
    ''' default flask development server '''
    import peewee
    import logging
    logger = logging.getLogger('peewee')
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())
    app.run(host=__HOST__, port=__PORT__, debug=True)

def profiling(levels=30):
    ''' flask dev. server, with added werkzeug profiling on every request '''
    from werkzeug.contrib.profiler import ProfilerMiddleware
    app.config['PROFILE'] = True
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[30])
    app.run(host=__HOST__, port=__PORT__, debug=True)

def gevent(*args):
    ''' gevent powered production deployment. '''
    from gevent.wsgi import WSGIServer
    s = WSGIServer((__HOST__, __PORT__), app)
    s.serve_forever()

DEPLOYS = {'dev': dev,
           'profiling': profiling,
           'profile': profiling,
           'waitress': waitress,
           'gevent': gevent}

# And start the correct server

if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] in DEPLOYS:
        RUNNER = DEPLOYS[sys.argv[1]]
        print(RUNNER.__doc__)
        RUNNER(*sys.argv[2:])
    else:
        print("Starting Development Server...")
        dev(*sys.argv[1:])

