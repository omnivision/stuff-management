'''
    First file on the noble epic task of unit testing.
'''

import sys
import os
import tempfile
import unittest
from flask import json

sys.path.append(os.path.dirname(__file__) + '/..')

import stuff_management.models as models

# pylint: disable=too-many-public-methods

class DatabaseTestCase(unittest.TestCase):
    ''' Base Class, initialises and tears down a database context. '''

    def setUp(self):
        ''' initialise temporary new database. '''

        models.create_all(':memory:') # also automatically does the DB.init()

    def tearDown(self):
        ''' delete temporary database '''
        models.DB.close()

class TestSetup(DatabaseTestCase):
    ''' First basic sanity checks '''

    def test_empty_db(self):
        pass

    def test_create_location(self):
        l = models.Location.create()
        l.save()

    def test_action_create_location(self):
        l = models.Location.create()
        l.actionFirstSave()

        total_actions = 0

        for i in models.UndoAction.select():
            self.assertEquals(i.table, 'Location')
            self.assertEquals(i.row, l.id)
            self.assertEquals(json.loads(i.before)['exists'], False)
            total_actions += 1

        assert total_actions == 1

    def test_update_location_name_and_existance(self):
        ''' update a location, and change it,
            and test undoing works correctly. '''

        l = models.Location.create()
        l.actionFirstSave()

        l.actionUpdateSave(name='booya')

        self.assertEquals(l.name, 'booya')
        
        actions = [a for a in models.UndoAction.select()]
        
        self.assertEquals(len(actions), 2)

        self.assertEquals(actions[0].table, 'Location')
        self.assertEquals(actions[0].row, l.id)
        self.assertEquals(json.loads(actions[0].before), {'exists':False})

        self.assertEquals(actions[1].table, 'Location')
        self.assertEquals(actions[1].row, l.id)
        self.assertEquals(json.loads(actions[1].before), {'name': 'New'})

        actions[1].undo()

        l = models.Location.get(models.Location.id == actions[1].row)
        self.assertEquals(l.name, 'New')
        self.assertEquals(l.exists, True)

        actions[0].undo()

        l = models.Location.get(models.Location.id == actions[1].row)
        self.assertEquals(l.name, 'New')
        self.assertEquals(l.exists, False)

    def test_model_relationships_undo(self):
        ''' create and update a location's parent field, and test that
            undoing still works. '''

        # p = parent, c = child

        p = models.Location.create()
        p.actionFirstSave()

        c = models.Location.create()
        c.actionFirstSave()

        cid = c.id

        undo_1 = c.actionUpdateSave(parent=p)

        print undo_1.before

        c = models.Location.get(models.Location.id == cid)
        self.assertEquals(c.parent, p)

        undo_2 = undo_1.undo()

        print undo_2.before

        c = models.Location.get(models.Location.id == cid)
        self.assertEquals(c.parent, None)

        undo_3 = undo_2.undo()

        print undo_3.before

        c = models.Location.get(models.Location.id == cid)
        self.assertEquals(c.parent, p)
