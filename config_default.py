DB='database.db'

# these values are sent to all templates:
SITE_VARS={
    'site_title': 'Stuff Management!',
    'issue_texts': ['Note', 'Fault', 'Broken beyond repair', 'Missing', 'Needs Cleaning', 'Needs maintenance'],
    }
