#!.virtualenv/bin/python -i

from stuff_management.models import *
import config

import sys

if len(sys.argv) == 2 and sys.argv[1] == 'create':
    create_all(config.DB)
else:
    DB.init(config.DB)
