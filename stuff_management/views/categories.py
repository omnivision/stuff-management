# -*- coding: utf-8 -*-
#     Stuff Management Event management inventory system
#     (C) Copyright 2014 Daniel Fairhead
#
#    Stuff Management is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Stuff Management is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Stuff Management.  If not, see <http://www.gnu.org/licenses/>.
#
#    ---------------------------------

"""
    stuff_management.views.categories
    ---------------------------------------
    Views for editing feeds and posts.

"""

from flask import render_template, url_for, request, redirect, \
                  flash, json, jsonify
from stuff_management import app
from stuff_management.models import Category

####################################################################

@app.route('/categories', methods=['GET', 'POST'])
def categories():
    ''' Basic HTML list of categories.  Also used (with POST) to create
        new initial categories '''

    if request.method == 'POST':
        new_cat = Category.create()
        new_cat.parent = request.form.get('parent', None)
        new_cat.actionFirstSave()
        return redirect(request.referrer)

    return render_template('categories.html',
                           category=None,
                           categories=Category.select() \
                                     .where((Category.parent>>None) \
                                           and Category.exists==True))

@app.route('/categories/<int:cat_id>', methods=['GET','POST','DELETE'])
def category(cat_id):
    ''' Basic HTML view of a category (and subcategories). '''

    cat = Category.get(Category.id == cat_id)
    return render_template('categories.html',
                           category=cat, categories=cat.subcategories)


@app.route('/api/categories/<int:cat_id>', methods=['GET', 'POST', 'DELETE'])
def api_category(cat_id):
    ''' the API / JSON view of a category. '''

    cat = Category.get(Category.id == cat_id)

    if request.method == 'DELETE':
        cat.actionUpdateSave(exists=False)
        return jsonify({'action':'delete','status':'done'})

    if request.method == 'POST':
        cat.process_form(request.form)
        return jsonify({'action': 'update', 'status':'done','data':request.form})
