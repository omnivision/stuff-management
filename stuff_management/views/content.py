# -*- coding: utf-8 -*-
#     Stuff Management Event management inventory system
#     (C) Copyright 2014 Daniel Fairhead
#
#    Stuff Management is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Stuff Management is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Stuff Management.  If not, see <http://www.gnu.org/licenses/>.
#
#    ---------------------------------

"""
    stuff_management.views.contents
    ---------------------------------------
    Views for editing box contents. 

"""

from flask import render_template, url_for, request, redirect, \
                  flash, json, jsonify
from stuff_management import app
from stuff_management.models import Content, Box, BoxXRef

####################################################################

@app.route('/contents', methods=['GET', 'POST'])
def contents():
    ''' Basic HTML list of contents.  Also used (with POST) to create
        new initial contents '''

    if request.method == 'POST':
        new_content = Content.create()

        # this is done this rather awkward way so that if '' is sent as the cat
        # or box, it still kind of works.

        cat = request.form.get('category', None)
        if cat:
            new_content.category = cat
        box = request.form.get('box', None)
        if box:
            new_content.box = box

        new_content.actionFirstSave()

        return redirect(request.referrer)

    return render_template('contents.html',
                           content=None,
                           contents=Content.select().where(Content.exists==True))

@app.route('/contents/<int:content_id>', methods=['GET','POST','DELETE'])
def content(content_id):
    ''' Basic HTML view of a content (and subcontents). '''

    content = Content.get(Content.id == content_id)
    return render_template('contents.html',
               content=content,
               )

@app.route('/api/contents')
def api_contents():
    ''' the API / JSON list view of all contents '''
    return jsonify({'contents': [
        {'id': c.id,
        'name': c.name,
        'model': c.model_number,
        'serial': c.serial_number}
        for c in Content.select().where(Content.exists == True) ]})

@app.route('/api/contents/<int:content_id>', methods=['GET', 'POST', 'DELETE'])
def api_content(content_id):
    ''' the API / JSON view of a content. '''

    content = Content.get(Content.id == content_id)

    if request.method == 'DELETE':
        content.actionUpdateSave(exists=False)
        return jsonify({'action':'delete','status':'done'})

    if request.method == 'POST':
        content.process_form(request.form)
        return jsonify({'action': 'update', 'status':'done','data':request.form})
