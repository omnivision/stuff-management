'''
    Search functionality
'''

from flask import render_template, g, request, stream_with_context, Response, url_for

from stuff_management.models import Category, Box, Location, Content

from stuff_management import app

@app.route('/search', methods=['GET', 'POST'])
def search():
    ''' basic search.  Eventually will return JSON or HTML. Currently
        just returns HTML (a <li> type list). Works as a 'yield' format,
        so that for huge databases it should be a little quicker.  I don't
        know if that's actually true... It might just be a lot simpler
        to switch to templates...'''

    query = request.form.get('query')
    if not query:
        return ''

    @stream_with_context
    def search_gen():
        boxes = []
        yield '<li role="presentation" class="dropdown-header">Boxes</li>'

        for box in Box.select().where(Box.uglycode.contains(query)):
            boxes.append(box.id)
            yield '<li><a href="%s"><b>%s</b> %s</a></li>' % (url_for('box', box_id=box.id), box.uglycode, box.name)

        for box in Box.select().where(Box.name.contains(query) & ~(Box.id << boxes)):
            yield '<li><a href="%s">%s</a></li>' % (url_for('box', box_id=box.id), box.name)

        yield '<li role="presentation" class="dropdown-header">Categories</li>'

        for cat in Category.select().where(Category.name.contains(query)):
            yield '<li><a href="%s">%s</a></li>' % (url_for('category', cat_id=cat.id), cat.name)

        yield '<li role="presentation" class="dropdown-header">Box Contents</li>'

        contents = []

        for content in Content.select().where(Content.name.contains(query)):
            contents.append(content.id)
            yield '<li><a href="%s">%s</a></li>' % (url_for('content', content_id=content.id), content.name)

        for content in Content.select().where(Content.model_number.contains(query) &
                                              ~(Content.id << contents)):
            yield '<li><a href="%s">%s</a></li>' % (url_for('content', content_id=content.id), content.name + '(' + content.model_number + ')')




    return Response(search_gen())
