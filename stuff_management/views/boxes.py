# -*- coding: utf-8 -*-
#     Stuff Management Event management inventory system
#     (C) Copyright 2014 Daniel Fairhead
#
#    Stuff Management is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Stuff Management is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Stuff Management.  If not, see <http://www.gnu.org/licenses/>.
#
#    ---------------------------------

"""
    stuff_management.views.boxes
    ---------------------------------------
    Views for editing feeds and posts.

"""

from flask import render_template, url_for, request, redirect, \
                  flash, json, jsonify, Response
import csv

from stuff_management import app
from stuff_management.models import Box, BoxXRef, Content, Location, Category

####################################################################

@app.route('/boxes', methods=['GET', 'POST'])
def boxes():
    ''' Basic HTML list of boxes.  Also used (with POST) to create
        new initial boxes '''

    if request.method == 'POST':
        new_box = Box.create()
        new_box.category = request.form.get('category', None)
        new_box.home = request.form.get('location', None)
        new_box.actionFirstSave()
        if 'parent' in request.form:
            box_xref = BoxXRef(parent=request.form['parent'], child=new_box)
            box_xref.save()

        return redirect(request.referrer)

    return render_template('boxes.html',
                           box=None,
                           boxes=Box.select() #Box, Category, Location)
                               #.join(Category)
                               #.switch(Box).join(Location)
                              )


@app.route('/boxes/<int:box_id>', methods=['GET','POST','DELETE'])
def box(box_id):
    ''' Basic HTML view of a box (and subboxes). '''

    box = Box.get(Box.id == box_id)

    if request.method == 'POST':
        box.process_form(request.form)

        if request.form.get('require_box', False):
            box_xref = BoxXRef(parent=box, child=Box.get(Box.id == request.form['require_box']))
            box_xref.save()


    # hacky way to add  'next item' button. Essentially try and guess what the
    # next 'uglycode' might be, and try and hunt for a next one.
    try:
        uglycodenum = int(box.uglycode.strip('ABCDEFGHIJKLMNOPQURSTUVWXYZ'))
        uglycodealp = box.uglycode.strip('0123456789')
        nextboxid = (Box().select(Box.id).where(Box.uglycode <<
                        [uglycodealp + str(i) for i in xrange(uglycodenum+1, uglycodenum+20)])
                        .order_by(Box.uglycode)
                        .tuples().get())[0]
    except Exception as e:
        nextboxid = None


    return render_template('boxes.html',
                           box=box,
                           required_boxes=(b.child for b in box.boxes_i_need),
                           dependent_boxes=(b.parent for b in box.boxes_that_need_me),
                           required_cats=(c.child for c in box.cats_i_need),
                           xref_api=lambda c:url_for('api_box_xref', box_id=box.id, child_id=c),
                           next_id=nextboxid,
                           )

@app.route('/api/boxes', methods=['GET', 'POST', 'DELETE'])
def api_boxes():
    ''' the API / JSON view of boxes (plural), and creating new boxes '''
    if request.method == 'POST':
        new_box = Box.create()
        new_box.process_form(request.form)
        new_box.save()

        return jsonify({'action': 'create',
                        'id': new_box.id,
                        'name': new_box.name})
    else:
        return jsonify({'boxes': [
            {'id': box.id,
            'name': box.name,
            'uglycode': box.uglycode,
            'uri': url_for('api_box', box_id=box.id),
            'edit_url': url_for('box', box_id=box.id)
            }
            for box in Box.select().where(Box.exists==True)]})

@app.route('/api/boxes.csv')
def api_boxes_csv():
    ''' very simple CSV list / view of all boxes for use with a label
        printer thing. Can be optimised, still has N+1 queries... '''
    class FakeFile(object):
        def __init__(self):
            self.lines = []
        def write(self, text):
            self.lines.append(text)
        def output(self):
            return ''.join(self.lines)

    fakefile = FakeFile()
    writer = csv.writer(fakefile)

    writer.writerow(['Code', 'Name', 'Location'])
    for box in Box.select().where(Box.exists == True):
        writer.writerow([box.uglycode, box.name, box.home.name if box.home else ''])

    return Response(response=fakefile.output(),
                    status=200,
                    mimetype='text/csv')

@app.route('/api/boxes/<int:box_id>', methods=['GET', 'POST', 'DELETE'])
def api_box(box_id):
    ''' the API / JSON view of a box. '''

    box = Box.get(Box.id == box_id)

    if request.method == 'DELETE':
        box.actionUpdateSave(exists=False)
        return jsonify({'action':'delete','status':'done'})

    if request.method == 'POST':
        box.process_form(request.form)

        if request.form.get('require_box', False):
            box_xref = BoxXRef(parent=box, child=Box.get(Box.id == request.form['require_box']))
            box_xref.save()

        return jsonify({'action': 'update', 'status':'done','data':request.form})

@app.route('/api/boxes/<int:box_id>/xref/<int:child_id>',
           methods=['GET', 'POST', 'DELETE'])
def api_box_xref(box_id, child_id):

    try:
        xref = BoxXRef.get((BoxXRef.parent==box_id)&(BoxXRef.child==child_id))
    except BoxXRef.DoesNotExist as e:
        if request.method=='POST':
            xref = BoxXRef(parent=box_id, child=child_id)
            xref.save()

    if request.method=='DELETE':
        xref.delete_instance()
        return jsonify({'action': 'delete', 'status':'done', 'data': request.form})

    return jsonify({"box_xref": {
                        "parent":{"id": box_id,
                                  "uri": url_for('api_box', box_id=box_id)},
                        "child":{"id": child_id,
                                 "uri": url_for('api_box', box_id=child_id)},
                        "relationship": xref.relationship}})


@app.route('/print/boxes/')
def print_boxes():
    return render_template('print_boxes.html',
                       boxes=Box.select().where(Box.exists==True),
                       options=request.args)


