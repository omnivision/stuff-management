# -*- coding: utf-8 -*-
#     Stuff Management Event management inventory system
#     (C) Copyright 2014 Daniel Fairhead
#
#    Stuff Management is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Stuff Management is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Stuff Management.  If not, see <http://www.gnu.org/licenses/>.
#
#    ---------------------------------

"""
    stuff_management.views.multiitems
    ---------------------------------------
    Views for editing feeds and posts.

"""

from flask import render_template, url_for, request, redirect, \
                  flash, json, jsonify
from stuff_management import app
from stuff_management.models import MultiItem, Box, BoxXRef

####################################################################

@app.route('/multiitems', methods=['GET', 'POST'])
def multiitems():
    ''' Basic HTML list of multiitems.  Also used (with POST) to create
        new initial multiitems '''

    if request.method == 'POST':
        new_multiitem = MultiItem.create()

        cat = request.form.get('category', None)
        if cat:
            new_multiitem.category = cat
        box = request.form.get('box', None)
        if box:
            new_multiitem.box = box

        new_multiitem.actionFirstSave()

        return redirect(request.referrer)

    return render_template('multiitems.html',
                           multiitem=None,
                           multiitems=MultiItem.select().where(MultiItem.exists==True))

@app.route('/multiitems/<int:multiitem_id>', methods=['GET','POST','DELETE'])
def multiitem(multiitem_id):
    ''' Basic HTML view of a multiitem (and submultiitems). '''

    multiitem = MultiItem.get(MultiItem.id == multiitem_id)
    return render_template('multiitems.html',
               multiitem=multiitem,
               )


@app.route('/api/multiitems/<int:multiitem_id>', methods=['GET', 'POST', 'DELETE'])
def api_multiitem(multiitem_id):
    ''' the API / JSON view of a multiitem. '''

    multiitem = MultiItem.get(MultiItem.id == multiitem_id)

    if request.method == 'DELETE':
        multiitem.actionUpdateSave(exists=False)
        return jsonify({'action':'delete','status':'done'})

    if request.method == 'POST':
        multiitem.process_form(request.form)
        return jsonify({'action': 'update', 'status':'done','data':request.form})
