'''
    Stuff Management main / initial views module.
'''

#pylint: disable=global-statement

from flask import render_template, g, request, stream_with_context, Response

import stuff_management.views.locations
import stuff_management.views.categories
import stuff_management.views.boxes
import stuff_management.views.content
import stuff_management.views.multiitems
import stuff_management.views.events
import stuff_management.views.search
import stuff_management.views.issues

from stuff_management import app
from stuff_management.models import DB, Category, Box, Location

import barcode

__all_categories__ = None

def all_categories(top_level_only=False):
    ''' returns an iterable for giving a list of all categories. useful
        in templates for select boxes, etc. '''
    global __all_categories__
    if top_level_only:
        return Category.select().where((Category.exists == True) &
                                       (Category.parent >> None))
    else:
        if not __all_categories__:
            cats = []
            level = [0]

            def add_children(cat):
                ''' add child categories with levels to single list... '''
                cats.append({"id": cat.id, "name": cat.name, "level": level[0]})
                level[0] += 1
                for c in cat.subcategories:
                    add_children(c)
                level[0] -= 1

            for c in all_categories(True):
                add_children(c)

            __all_categories__ = cats
        return __all_categories__


__all_locations__ = None

def all_locations(top_level_only=False):
    ''' returns an iterable for giving a list of all locations. useful
        in templates for select boxes, etc. '''
    global __all_locations__

    if top_level_only:
        return Location.select().where((Location.exists == True) &
                                       (Location.parent >> None))
    else:
        if not __all_locations__:

            locations = []
            level = [0]

            def add_children(location):
                ''' add child locations with levels to single list... '''
                locations.append({"id": location.id,
                                  "name": location.name,
                                  "level": level[0]})
                level[0] += 1
                for l in location.sublocations:
                    add_children(l)
                level[0] -= 1

            for l in all_locations(True):
                add_children(l)

            __all_locations__ = locations

        return __all_locations__


def all_boxes():
    ''' returns an iterable for giving a list of all categories. useful
        in templates for select boxes, etc. '''
    return Box.select().where(Box.exists == True)


################################################################################

@app.before_request
def before_request():
    ''' load some global vars for template use, and init the DB '''
    g.site_vars = app.config.get('SITE_VARS')

    DB.init(app.config.get('DB'))
    DB.connect()

    # and add some useful basic functions to 'g', for use in templates:

    g.all_categories = all_categories
    g.all_locations = all_locations
    g.all_boxes = all_boxes

@app.teardown_request
def after_request(exception):
    ''' after a request.... close the DB '''
    global __all_categories__, __all_locations__
    DB.close()


    __all_categories__ = None
    __all_locations__ = None


@app.route('/')
def index():
    ''' front page. '''
    return render_template('index.html')

@app.route('/api/barcode/<code>')
def api_barcode(code):
    code = filter(lambda t: t in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890', code)
    code39 = barcode.get_barcode_class('code39')(code, add_checksum=False).render(None)
    return Response(code39, content_type='image/svg+xml')
