# -*- coding: utf-8 -*-
#     Stuff Management Event management inventory system
#     (C) Copyright 2014 Daniel Fairhead
#
#    Stuff Management is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Stuff Management is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Stuff Management.  If not, see <http://www.gnu.org/licenses/>.
#
#    ---------------------------------

"""
    stuff_management.views.locations
    ---------------------------------------
    Views for editing feeds and posts.

"""

from flask import render_template, url_for, request, redirect, \
                  flash, json, jsonify
from stuff_management import app
from stuff_management.models import Location

####################################################################

@app.route('/locations', methods=['GET', 'POST'])
def locations():
    ''' Basic HTML list of locations.  Also used (with POST) to create
        new initial locations '''

    if request.method == 'POST':
        new_location = Location.create()
        new_location.parent = request.form.get('parent', None)
        new_location.actionFirstSave()
        return redirect(request.referrer)

    return render_template('locations.html',
                           location=None,
                           locations=Location.select() \
                                     .where(Location.parent>>None) )

@app.route('/locations/<int:location_id>', methods=['GET','POST','DELETE'])
def location(location_id):
    ''' Basic HTML view of a location (and sublocations). '''

    loc = Location.get(Location.id == location_id)

    return render_template('locations.html',
                           location=loc, locations=loc.sublocations)


@app.route('/api/locations/<int:location_id>', methods=['GET', 'POST', 'DELETE'])
def api_location(location_id):
    ''' the API / JSON view of a location. '''

    loc = Location.get(Location.id == location_id)

    if request.method == 'DELETE':
        loc.actionUpdateSave(parent=None, exists=False)
        return jsonify({'action':'delete','status':'done'})

    if request.method == 'POST':
        loc.process_form(request.form)
        return jsonify({'action': 'update', 'status':'done','data':request.form})
