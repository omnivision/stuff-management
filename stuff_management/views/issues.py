# -*- coding: utf-8 -*-
#     Stuff Management Event management inventory system
#     (C) Copyright 2014 Daniel Fairhead
#
#    Stuff Management is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Stuff Management is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Stuff Management.  If not, see <http://www.gnu.org/licenses/>.
#
#    ---------------------------------

"""
    stuff_management.views.issues
    ---------------------------------------
    Views for editing issues

"""

from flask import render_template, url_for, request, redirect, \
                  flash, json, jsonify
from flask.ext import restful
from flask.ext.restful import fields, marshal, marshal_with
from stuff_management import app, api
from stuff_management.models import Issue, Box, Content, MultiItem, Event

####################################################################

@app.route('/issues', methods=['GET', 'POST'])
def issues():
    if request.method == 'POST':
        issue = Issue(exists=True, status='new')
        issue.save()

    return render_template('issues.html',
                issues=Issue.select())

@app.route('/issues/<int:issue_id>', methods=['GET', 'POST', 'DELETE'])
def issue(issue_id):
    pass


##############################################################################

class Date(fields.Raw):
    def format(self, value):
        return value.isoformat()

class NormalURL(fields.Raw):
    def __init__(self, endpoint, url_key, attr=None):
        super(NormalURL, self).__init__()
        self.endpoint = endpoint
        self.attr = attr
        self.url_key = url_key

    def output(self, key, obj):
        key = key if not self.attr else self.attr
        try:
            return url_for(self.endpoint, **{self.url_key: getattr(obj, key)})
        except AttributeError:
            return url_for(self.endpoint, **{self.url_key: obj[key]})

class RelatedType(fields.Raw):
    def output(self, key, obj):
        if obj.box:
            return 'box'
        elif obj.item:
            return 'content'
        elif obj.multiitem:
            return 'mulititem'
        else:
            return 'dunno'

##############################################################################

issue_fields = {
    'id': fields.Integer,
    'uri': NormalURL('APIIssue', 'issue_id', 'id'),
    'page': NormalURL('issue', 'issue_id', 'id'),
    'type': RelatedType,
    'name': fields.String,
    'exists': fields.Boolean,
    'resolved': fields.Boolean,
    'opened_date': Date,
    'resolved_date': Date,
    'status': fields.String,
}

issues_fields = {
    'issues': fields.List(fields.Nested(issue_fields))
    }


class APIIssues(restful.Resource):
    ''' List of Issues '''
    @marshal_with(issues_fields)
    def get(self):
        return {'issues': Issue.select()}

    @marshal_with(issue_fields)
    def post(self):
        issue = Issue()
        issue.status="new"

        if request.form['rel_type'] == 'box':
            issue.box = Box.get(Box.id == request.form['rel_id'])
        elif request.form['rel_type'] == 'content':
            issue.item = Content.get(Content.id == request.form['rel_id'])
        elif request.form['rel_type'] == 'multiitem':
            issue.multiitem = MultiItem.get(MultiItem.id == request.form['rel_id'])
        elif request.form['rel_type'] == 'event':
            issue.event = Event.get(Event.id == request.form['rel_id'])
        else:
            raise Exception('Unknown Type for issue')

        issue.actionFirstSave()
        return issue


class APIIssue(restful.Resource):
    @marshal_with(issue_fields)
    def get(self, issue_id):
        return Issue.get(Issue.id == issue_id)

    @marshal_with(issue_fields)
    def put(self, issue_id):

        issue = Issue.get(Issue.id == issue_id)
        updates = dict(request.form.items())
        issue.actionUpdateSave(**updates)

        return issue

    @marshal_with(issue_fields)
    def delete(self, issue_id):
        issue = Issue.get(Issue.id == issue_id)
        issue.actionUpdate(exists=False)
        issue.save()

        return issue


api.add_resource(APIIssues, '/api/issues', endpoint='APIIssues')
api.add_resource(APIIssue, '/api/issues/<int:issue_id>', endpoint='APIIssue')

