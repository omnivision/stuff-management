# -*- coding: utf-8 -*-
#     Stuff Management Event management inventory system
#     (C) Copyright 2014 Daniel Fairhead
#
#    Stuff Management is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Stuff Management is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Stuff Management.  If not, see <http://www.gnu.org/licenses/>.
#
#    ---------------------------------

"""
    stuff_management.views.events
    ---------------------------------------
    Views for editing events & packing lists

"""

from flask import render_template, url_for, request, redirect, \
                  flash, json, jsonify
from flask.ext import restful
from flask.ext.restful import fields, marshal, marshal_with
from stuff_management import app, api
from stuff_management.models import Event, EquipmentList

####################################################################

@app.route('/events', methods=['GET', 'POST'])
def events():
    if request.method == 'POST':
        event = Event(status='new')
        event.actionFirstSave(exists=True)

    return render_template('events.html',
                events=Event.select())

@app.route('/events/<int:event_id>', methods=['GET', 'POST', 'DELETE'])
def event(event_id):
    try:
        evt = Event.get(id=event_id)
    except:
        flash('Invalid Event id!')
        return redirect(url_for(events))

    if request.method == 'POST':
        evt.process_form(request.form)

    return render_template('event.html',
            can_delete=True, # TODO!!!
            event=evt)


@app.route('/packinglists/', methods=['POST'])
def packinglists():
    try:
        p = EquipmentList(event=int(request.form['event']))
        p.actionFirstSave()
    except Exception as e:
        print e
        flash('Failed to create new packinglist!')
    return redirect(url_for('event', event_id=request.form['event']))

@app.route('/packinglists/<int:list_id>', methods=['GET','PUT','DELETE'])
def packinglist(list_id):
    try:
        p = EquipmentList.get(id=list_id)
    except:
        flash('invalid packinglist id')
        return redirect('/events/')

    if request.method == 'PUT':
        data = json.loads(request.data)
        p.process_form(data)
        p.save()

    return render_template('packinglist.html', packinglist=p)

##############################################################################

class Date(fields.Raw):
    def format(self, value):
        return value.isoformat()

class NormalURL(fields.Raw):
    def __init__(self, endpoint, url_key, attr=None):
        super(NormalURL, self).__init__()
        self.endpoint = endpoint
        self.attr = attr
        self.url_key = url_key

    def output(self, key, obj):
        key = key if not self.attr else self.attr
        try:
            return url_for(self.endpoint, **{self.url_key: getattr(obj, key)})
        except AttributeError:
            return url_for(self.endpoint, **{self.url_key: obj[key]})

##############################################################################

event_fields = {
    'id': fields.Integer,
    'uri': NormalURL('APIEvent', 'event_id', 'id'),
    'page': NormalURL('event', 'event_id', 'id'),
    'name': fields.String,
    'exists': fields.Boolean,
    'start_date': Date,
    'end_date': Date
}

events_fields = {
    'events': fields.List(fields.Nested(event_fields))
    }

########################
# packing lists:

#  TODO??? Or not? just leave as JSON and all the work client-side?

#packingsection_fields = {
    #'title': fields.String,
    #'items': fields.List(fields.Nested(packingitem_fields))
#}

#packinglist_fields = {
    #'id': fields.Integer,
    #'name': fields.String,
    #'packed': fields.Boolean,
    #'event_id': fields.Integer,
    #}


class APIEvents(restful.Resource):
    ''' List of Events '''
    @marshal_with(events_fields)
    def get(self):
        return {'events': Event.select()}

    @marshal_with(event_fields)
    def post(self):
        event = Event()
        event.status="new"
        event.actionFirstSave()
        return event


class APIEvent(restful.Resource):
    @marshal_with(event_fields)
    def get(self, event_id):
        return Event.get(Event.id == event_id)

    @marshal_with(event_fields)
    def put(self, event_id):

        event = Event.get(Event.id == event_id)
        event.actionUpdateSave(**request.form.to_dict())

        return event

    def post(self, event_id):
        return self.put(event_id)

    @marshal_with(event_fields)
    def delete(self, event_id):
        event = Event.get(Event.id == event_id)
        event.actionUpdateSave(exists=False)

        return event


api.add_resource(APIEvents, '/api/events', endpoint='APIEvents')
api.add_resource(APIEvent, '/api/events/<int:event_id>', endpoint='APIEvent')

