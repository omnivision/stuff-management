# -*- coding: utf-8 -*-
"""  Stuff Management Digital Signage Project
     (C) Copyright 2013 Daniel Fairhead

    Stuff Management is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Stuff Management is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Stuff Management.  If not, see <http://www.gnu.org/licenses/>.

"""

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from flask import Flask
import flask
from flask.ext.restful import Api

try:
    import config
except:
    print("Config file missing!!!")
    import config_default

app = Flask(__name__) # pylint: disable=invalid-name
app.config.from_object('config')

api = Api(app)

import models
import stuff_management.views as views
