/**
 * jquery.biglist.js (C) 2014 Daniel Fairhead. MIT Licenced.
 **/
(function($) {
    "use strict";

    $.fn.biglist = function (template) {
        var tmpl_html = $(template).html(),
            that = this,
            initial_handler = (function () {
                var selectbox = $(this),
                    real = selectbox.children('option:selected')[0];
                if (! selectbox.data('_biglist_loaded')) {
                    selectbox.data('_biglist_loaded', true);

                    selectbox.html(tmpl_html);
                    selectbox.children('option[value=' + real.value + ']').prop('selected', true);
                }
            });

        this.on('mouseenter', initial_handler);

        return this;
    }

}(jQuery));
