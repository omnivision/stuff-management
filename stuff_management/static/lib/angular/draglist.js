'use strict';
///////////////////////////////////////
// draglist.js
// (C) 2014 Daniel Fairhead
// MIT License
///////////////////////////////////////
// Usage:
// <draglist>
//	<dragitem ng-repeat="thing in list">
//	  {{ thing.name }}
//	</dragitem>
// </draglist>

(function(){
"use strict";
var dragDrop={};

angular.module('dragList', [])
.directive('draglist', ['$parse', function($parse) {
var dropEvent=null;

// The 'list' component.
return {
	restrict: 'E',
	transclude: true,

	scope: {
		drop:'&',
		id:'@',
		allowDrops:'@',
		draglist:'='
	},

	controller: function($scope, $element) {
		var draglist = $element[0];

		if ($scope.allowDrops !== 'no') {
			draglist.addEventListener('dragover', function(e) {
			$element.addClass('draglist-hover');
				if (e.preventDefault) {
					e.preventDefault();
				}
				e.dataTransfer.dropEffect = 'move';
			}, false);

			draglist.addEventListener('drop', function(e) {
				dragDrop.landedList = $scope.draglist;
				$element.removeClass('draglist-hover');
				$scope.$apply(function() {
					$scope.drop(e, $element, dragDrop);
				});
			}, false);
		}

		draglist.addEventListener('dragstart', function(e) {
			dragDrop.fromList = $scope.draglist;
			e.dataTransfer.setData('draglist/id', $scope.id);
			console.log($element);
			window.x = $element;
		}, false);

		draglist.addEventListener('dragleave', function(e){
			$element.removeClass('draglist-hover');
		}, false);
	},

	link:  function($scope, $element, $attrs) {
		dropEvent = $scope.drop;
	},
	template:
	'<div><style>'
//	+'.dragitem-hover { border-top: 1em solid #222; }'
	+'.dragitem-hover::before {content:"Drop Here";border:1px dashed #333;display:block;padding:0.8em;background:#eee}'
	+'.draglist{ min-height:2em;}'
	+'.draglist-hover{ border:1px dashed #333}</style>'
	+'<div class="list-group draglist" ng-transclude></div></div>',
	replace: true
}}])
.directive('dragitem',['$parse', function($parse) {
// The list item component within a list.
return {
//require: '^draglist',
	restrict: 'E',
	transclude: true,
	scope: {
		relatedObject: '=obj',
		ind: '=ind',
		},
	template: '<div class="list-group-item" ng-transclude></div>',
	link: function($scope, $element, $attrs) {
		var el = $element[0];

		el.addEventListener('dragenter', function(e) {
			$element.addClass('dragitem-hover');
		}, false);
		el.addEventListener('dragleave', function(e) {
			if ($(e.relatedTarget).parents('.list-group-item')[0] == $element[0]) {
				return;
			}
			$element.removeClass('dragitem-hover');
		}, false);
		el.addEventListener('drop', function(e) {
			$element.removeClass('dragitem-hover');
			e.landedOn = $scope.relatedObject;
			dragDrop.landedOn = $scope.relatedObject;
		}, false);

		el.addEventListener('dragstart', function(e) {
			dragDrop = {};
			dragDrop.pickedUp = $scope.relatedObject;
			e.dataTransfer.effectAllowed = 'move';
			e.dataTransfer.setDragImage($element[0], 10, 10);
		}, false);

	},
	replace: true
};
}])
.value('dragDrop', function(){return dragDrop })
;})()
