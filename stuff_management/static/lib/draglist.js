'use strict';
///////////////////////////////////////
// draglist.js
// (C) 2014 Daniel Fairhead
// MIT License
///////////////////////////////////////
// Usage:
// <draglist>
//    <dragitem ng-repeat="thing in list">
//      {{ thing.name }}
//    </dragitem>
// </draglist>

(function(){
"use strict";
var current_drag=null;

angular.module('dragList', [])
.directive('draglist', ['$parse', function($parse) {
    var dropEvent=null;
// The 'list' component.
return {
    restrict: 'E',
    transclude: true,
    scope: { drop:'&',
        id:'@',
        allowDrops:'@'
    },
    controller: function($scope, $element) {
        var dragitem = $element[0];

        if ($scope.allowDrops !== 'no') {
            dragitem.addEventListener('dragover', function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                e.dataTransfer.dropEffect = 'move';
            }, false);

            dragitem.addEventListener('drop', function(e) {
                $scope.$apply(dropEvent())(e, $element, current_drag);
                console.log(current_drag);
            }, false);

        }

        dragitem.addEventListener('dragstart', function(e) {
            e.dataTransfer.setData('draglist/id', $scope.id);
        }, false);
    },

    link:  function($scope, $element, $attrs) {
        dropEvent = $scope.drop;
    },
    template:
    '<div><style>.dragitem-hover { border-top: 1em solid #222; }.draglist{min-height:2em;border-bottom: 2em solid gray;}</style>'
    +'<div class="list-group draglist" ng-transclude></div></div>',
    replace: true
}}])
.directive('dragitem',['$parse', function($parse) {
// The list item component within a list.
return {
    //require: '^draglist',
    restrict: 'E',
    transclude: true,
    scope: {
        relatedObject: '=obj',
        },
    template: '<div ng-transclude draggable="true" class="list-group-item"></div>',
    link: function($scope, $element, $attrs) {
        var el = $element[0];

        el.addEventListener('dragenter', function(e) {
            $element.addClass('dragitem-hover');
        }, false);
        el.addEventListener('dragleave', function(e) {
            $element.removeClass('dragitem-hover');
        }, false);
        el.addEventListener('drop', function(e) {
            $element.removeClass('dragitem-hover');
            e.landedOn = $scope.relatedObject;
        }, false);

        el.addEventListener('dragstart', function(e) {
            current_drag = $scope.relatedObject;
            e.dataTransfer.effectAllowed = 'move';
        }, false);

    },
    replace: true
};
}]);})()
