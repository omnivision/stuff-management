function CategoryModel(data) {
    var that=this;

    that.name = ko.observable(data['name']);
    that.description = ko.observable(data['description']);
    that.uglycode = ko.observable(data['uglycode']);

    that.abbr = ko.observable(data['abbr']);
    that.parent = ko.observable(data['parent']);
}
