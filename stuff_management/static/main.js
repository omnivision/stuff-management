(function(){
"use strict";

/* Magic for real time ajax stuff happening. */

$(document).on('click', '.item_ajax_btn', function() {
	var toggle_class = $(this).data('ajaxtoggle'),
		item = $(this).parents('.item').first().toggleClass(toggle_class),
		data = {};

	data[$(this).data('name')] = $(this).data('value');

	$.ajax($(this).parents('[data-uri]').data('uri'),
		   { type: $(this).data('ajaxtype'),
			 data: data,
			 error: function() {
				item.toggleClass(toggle_class);
				alert('failed to delete!');
				}})
});

function ajax_update(control, attr) {
	var uri = control.parents('[data-uri]').data('uri'),
		new_value = control[attr](),
		submit_type = control.parents('[data-update]').data('update'),
		data = {};

	if (control.data('previous') == new_value) {
		return;
	}

	// blasted DOM doesn't have 'name' attr for textareas,
	// so we have to check for data-name too...

	if (control.data('name') === undefined) {
		data[control.attr('name')] = new_value;
	} else {
		data[control.data('name')] = new_value;
	}

	if (! submit_type) {
		submit_type = 'POST';
	}

	$.ajax(uri, { type: submit_type,
				  data: data,
				  success: function() {
						control.data('previous', new_value);
						},
				  error: function() {
						control[attr](control.data('previous'));
						alert('Failed to change value!');
						}});
}

// Apply real-time updates to fields:

$(document).on('blur', '.item_ajax_control', function(){
	ajax_update($(this), 'val');
});

$(document).on('blur','.item_ajax_content_editable', function(){
	ajax_update($(this), 'text');
});


// TODO: checkboxes need to also do real time updates.

if ($.cookie('display-deleted')==='true') {
	$('body').append('<style id="hide-deleted-style">.deleted {display:none;}</style>');
}

$('#toggle-deleted').on('click', function() {
	$.cookie('display-deleted', !($.cookie('display-deleted')==='true'), { path:'/'});
	if ($.cookie('display-deleted')==='true') {
		$('body').append('<style id="hide-deleted-style">.deleted {display:none;}</style>');
	} else {
		$('#hide-deleted-style').remove();
	}
});

$(document).on('submit', '.ajax_section_form', function(event) {
    // If a form has the class 'ajax_section_form', then when it
    // is submitted, it will only reload the parent object with class 'section'
    // rather than the whole page.
    // The object with the class 'section' *MUST* have an id as well,
    // so that jQuery can figure out which bits to re-draw.

    event.preventDefault();

    var form = $(this),
        section = form.parents('.ajax_section').first();

    $.ajax( form.attr('action'), {
        data: form.serializeArray(),
        type: form.attr('method'),
        success: function(data) {
            window.s = section;
            window.d = data;
            section.replaceWith($(data).find('#' + section.attr('id')));
        }});
});

$(document).on('submit', '.ajax_section_form_api', function(event) {
    // Just like 'ajax_section_form_api', except for when working with
    // api (pure JSON) links, rather than the server-rendering versions.

    event.preventDefault();

    var form = $(this),
        section = form.parents('.ajax_section').first();

    $.ajax( form.attr('action'), {
        data: form.serializeArray(),
        type: form.attr('method'),
        success: function(data) {
            $(section).load(document.URL + ' #' + section.attr('id'));
        }});
});

$("#searchbox").on('keyup', function() {
    $('#searchdropdown').load('/search', {'query': $(this).val(), 'display':'html'});
    $('#searchdropdown').show();
});

$('.panel h3').on('click', function(e) {
    if (e.target !== this) { return true; }
    var hidden = $.cookie('collapsed-' + $(this).parents('.panel').attr('id')) === "true";
    $(this).toggleClass('hiding-stuff').parents('.panel-heading')
        .siblings().toggleClass('hidden');
    console.log(hidden);

    $.cookie('collapsed-' + $(this).parents('.panel').attr('id'), !hidden, { path:'/'});
});
$('.panel').each(function() {
    var id = $(this).attr('id');
    if ($.cookie('collapsed-' + id) === "true") {
        $(this).children(':not(.panel-heading)')
                .addClass('hidden');
        $(this).find('.panel-heading h3')
                .addClass('hiding-stuff');
    }
});

$('.datepicker').datepicker({format:'yyyy-mm-dd', showTodayButton:true}).on('changeDate', function(){
	ajax_update($(this), 'val');
});



$('.dropdown-menu select').click(function(e) { e.stopPropagation(); }); 


})();
