/*
 * events.js (C) 2014 Daniel Fairhead
 * part of "Stuff Management"
 * ---------------------------------------
 *  The Angular.js Application for Events
 * */
(function(){
"use strict";

angular.module('eventsApp', ['ngResource', 'ngRoute'])
	.factory('Event', ['$resource',
		function($resource) {
			return $resource(EVENTS_API + '/:id', {}, {
				query: {method:'GET', isArray:false}
			});
		}
	])
	.controller('eventsController', ['$scope', 'Event', function($scope, Event) {
		$scope.events = [];
		
		Event.query(function(r) {
			$scope.events = r.events;
		});

		$scope.addEvent = function() {
			$scope.events.push(Event.save({}, function(data) {
			}));
		};

		$scope.delEvent = function(ev) {
			Event.delete(ev);
			//$scope.events.splice($scope.events.indexOf(ev), 1);
			ev.exists = false;

		};
	}])
	.controller('eventController', ['$scope', '$routeParams', 'Event', function($scope, $routeParams, Event) {
		$scope.event = Event.get({id:$routeParams.id}, function(){
			// angular complains if input[type=date] is given string, not Date object as model...
			// this maybe should be in the Event resource object, not here...
			$scope.event.start_date = new Date($scope.event.start_date);
			$scope.event.end_date = new Date($scope.event.end_date);
		});
	}])
	.config(['$routeProvider',
		function($routeProvider) {
			$routeProvider.
				when('/events/:id', {
					templateUrl: '/static/templates/event.html',
					controller: 'eventController'
				}).
				otherwise({
					templateUrl: '/static/templates/events.html',
					controller: 'eventsController'
				})
		}]
	)
	;
})();
