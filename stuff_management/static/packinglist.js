/*
 * packinglist.js (C) 2014 Daniel Fairhead
 * part of "Stuff Management"
 * ---------------------------------------
 *  The Angular.js Application for packing lists.
 * */
(function(){
"use strict";

var INITIAL_BOXES_TO_HIDE=[];

Array.prototype.hasId = function(needle, field) {
	/* search an array for in object with field:id. */
	var i;
	if (!field) { field = 'id'}

	for (i=0;i<this.length;i++) {
		if (this[i][field] === needle) {
			return i;
		}
	}
	return false;
};


angular.module('packingListApp', ['ngResource', 'dragList'])
	.factory('inventoryController', function($resource) {
		return $resource(BOXES_API, {});
	})
	.controller('packingListController', ['$scope', '$http', 'inventoryController', function($scope, $http, inventoryController) {

    // First load data from page:
		$scope.sections = CURRENT_LIST?CURRENT_LIST:[];//[];
    // And tell module global to hide those items
    for(var s=0;s<CURRENT_LIST.length;s++){
      for (var i=0;i<CURRENT_LIST[s].items.length;i++){
        INITIAL_BOXES_TO_HIDE.push(CURRENT_LIST[s].items[i].id);
      }
    }

		$scope.extras = [
			{name: "extra notes", type: "extra", quantity: 9999},
			{name: "break", type: "extra", quantity: 9999},

			{name: "custom", type: "extra", quantity: 9999}
			];

		inventoryController.get(function(resp) {
			$scope.inventory = resp.boxes.filter(function(i){
        return INITIAL_BOXES_TO_HIDE.indexOf(i.id) === -1;
      });
		});

		$scope.save = function() {
      $http({
        url:window.location.toString(),
        method:'PUT',
        data: {data: $scope.sections},
      }).success(function(resp){
        alert('saved!');
      }).error(function(resp){
        alert('FAILED!!!');
      });
		}

		$scope.addSection = function() {
			$scope.sections.unshift( {
				title: "new section",
				items: []
			});
		}

		$scope.removeSection = function(section) {
			var i;

			for (i=section.items.length-1;i>=0;i--) {
				$scope.removeItem(section, section.items[i]);
			}

			$scope.sections.splice($scope.sections.indexOf(section), 1);

		};

		$scope.removeItem = function(section, item) {
			// This has to be here, not in the sectionListController, as it adds the items
			// back into the main inventory list.
			section.items.splice(section.items.indexOf(item), 1);

			if (item['type'] !== 'extra') {
				$scope.inventory.push(item);
			}
		};
	}])
	.controller('sectionListController', ['$scope', '$rootScope', 'dragDrop', function($scope, $rootScope, dragDrop) {
		var i=0,
			section = $scope.section,
			items = $scope.section.items;

		$scope.items = $scope.section.items;

		// item is dropped on this list.
		$scope.addItem = function(e, $element, current_drag) {//section, a, b, c) {

			var drag = dragDrop(),
				item = null;

			// remove pickedUp from previous list:
			if (drag.pickedUp.quantity > 1) {
				drag.pickedUp.quantity -= 1;
				item = {
					id: drag.pickedUp.name + Date.now(),
					name: drag.pickedUp.name,
					protoid: drag.pickedUp.id,
					quantity: 1,
					type: drag.pickedUp.type,
				}
				drag.pickedUp = item;
			} else {
				drag.fromList.splice(drag.fromList.indexOf(drag.pickedUp), 1);
			}

			if (drag.landedOn) {
				drag.landedList.splice(drag.landedList.indexOf(drag.landedOn), 0, drag.pickedUp);
			} else {
				drag.landedList.push(drag.pickedUp);
			}

		};

	$scope.toggleShowNotes = function(item) {
		item.showNotes = ! item.showNotes;
	};

	}])

})();
