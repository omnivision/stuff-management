'''
models.py - (C)2015 Daniel Fairhead, part of Stuff Management
------
Database models and functions using peewee ORM.

'''

import sys

#pylint: disable=unused-wildcard-import

from peewee import *
from datetime import datetime, timedelta
from flask import json

# TODO: move this elsewhere:
def datetime_converter(obj):
    ''' convert datetime objs to strings'''
    if isinstance(obj, datetime):
        return obj.isoformat()

def json_dumps_w_datetime(data):
    ''' dump an object to JSON, converting datetime to strings. '''
    return json.dumps(data, default=datetime_converter)

# ----------------

DB = SqliteDatabase(None, threadlocals=True)

class BaseModel(Model):
    ''' base class, holds DB meta info '''
    class Meta:
        database = DB


class UndoAction(BaseModel):
    ''' used to store changes to the database, so that a history is kept,
        and undoing can be done. '''

    time = DateTimeField(default=datetime.now())
    table = CharField()
    row = IntegerField()
    before = TextField()

    def undo(self):
        ''' select the item that was changed, change it back to what it was
            before, and make a new 'undo' item that contains these changes. '''
        model = getattr(sys.modules[__name__], self.table)
        instance = model.get(model.id == self.row)
        return instance.actionUpdateSave(**json.loads(self.before))

class NormalModel(BaseModel):
    ''' Used as base for most tables, contains usual fields, and data for
        undo support. '''

    abbr = CharField(default="")
    name = CharField(default="New")
    description = TextField(null=True)
    uglycode = CharField(default='')

    # item's usually aren't actually deleted by a 'delete' action,
    # instead this is set to false.  This means 'undo' stuff works a lot
    # easier.
    exists = BooleanField(default=False)

    def actionUpdate(self, **kwargs):
        ''' update fields, and create an undo action for it. '''
        updates = {}

        for k, v in kwargs.items():
            if hasattr(self, k):
                previous = getattr(self, k)
                if isinstance(previous, BaseModel):
                    previous = previous.id
                if previous != v:
                    updates[k] = previous
                    setattr(self, k, v)
                else:
                    print k, v, 'did not change'
            else:
                raise KeyError(k)

        if updates:
            action = UndoAction.create(table=self.__class__.__name__,
                                       row=self.id,
                                       before=json_dumps_w_datetime(updates))
            action.save()
            return action

    def actionFirstSave(self):
        self.exists = True
        self.save()
        action = UndoAction.create(table=self.__class__.__name__,
                                   row=self.id,
                                   before=u'{"exists": false}')
        action.save()
        return action

    def actionUpdateSave(self, **kwargs):
        action = self.actionUpdate(**kwargs)
        self.save()
        return action

    def process_common(self, formdata):
        print 'formdata:'
        print formdata
        updates = {}

        if 'abbr' in formdata:
            updates['abbr'] = formdata['abbr']
        if 'name' in formdata:
            updates['name'] = formdata['name']
        if 'description' in formdata:
            updates['description'] = formdata['description']
        if 'uglycode' in formdata:
            updates['uglycode'] = formdata['uglycode']
        if 'exists' in formdata:
            # boolean, but usually sent as text.
            updates['exists'] = formdata['exists']

        return updates

class Location(NormalModel):
    ''' where something can be stored '''

    parent = ForeignKeyField('self', related_name='sublocations', null=True)

    plan_picture = TextField(null=True)
    plan_location = TextField(null=True)

    # sublocations -> self

    def process_form(self, formdata):
        ''' this is usually used to update a category w/ ajax. '''
        updates = self.process_common(formdata)

        if 'plan_picture' in formdata:
            updates['plan_picture'] = formdata['plan_picture']
        if 'plan_location' in formdata:
            updates['plan_location'] = formdata['plan_location']


        if 'parent' in formdata:
            if formdata['parent'] == '':
                updates['parent'] = None
            else:
                updates['parent'] = formdata['parent']


        if updates:
            self.actionUpdateSave(**updates)


class Category(NormalModel):
    ''' how boxes, etc. are categorised '''

    parent = ForeignKeyField('self', related_name="subcategories", null=True)

    # subcategories -> self

    def process_form(self, formdata):
        ''' this is usually used to update a category w/ ajax. '''
        updates = self.process_common(formdata)

        if 'parent' in formdata:
            if formdata['parent'] == '':
                updates['parent'] = None
            else:
                updates['parent'] = formdata['parent']

        if updates:
            self.actionUpdateSave(**updates)

class Contact(NormalModel):
    ''' people, companies, event organisers, etc. '''
    first_name = CharField(default="")
    last_name = CharField(default="")

    is_human = BooleanField(default=True)

    street_address = TextField(default="")

    boss = ForeignKeyField('self', related_name='minions', null=True)

    language = TextField(default="")

    email_address = CharField(default="")

    other_details = TextField(default="{}") # JSON.

    # minions -> self
    # makes -> Content
    # supplies -> Content

################################################################################

class ItemModel(NormalModel):
    ''' boxes, contents, spares, and multiitems share a lot of common
        fields. '''

    barcode = CharField(default="")
    serial_number = CharField(default="")
    model_number = CharField(default="")
    url = CharField(default="")

    purchase_price = FloatField(default=0.0)
    new_price = FloatField(default=0.0)

    purchase_date = DateField(default=datetime.now())
    new_date = DateField(default=datetime.now())
    expected_eol = DateField(default=datetime.now() + timedelta(days=365*3))

    daily_rental = FloatField(default=0.0)
    hourly_rental = FloatField(default=0.0)
    weekly_rental = FloatField(default=0.0)

    manual = CharField(default="")
    links = TextField("{}") # JSON

    available = BooleanField(default=True)

    def process_common(self, formdata):
        ''' process all usual ItemModel fields. return dict '''
        updates = super(ItemModel, self).process_common(formdata)

        def basic(name):
            if name in formdata:
                updates[name] = formdata[name]

        basic('barcode')
        basic('serial_number')
        basic('model_number')
        basic('url')
        basic('purchase_price')
        basic('new_price')
        basic('purchase_date')
        basic('new_date')
        basic('expected_eol')
        basic('daily_rental')
        basic('hourly_rental')
        basic('weekly_rental')
        basic('manual')
        basic('available')

        if 'links' in formdata:
            updates['links'] = json.dumps(json.loads(formdata['links']))

        return updates

    def process_form(self, formdata):
        ''' this is usually used to update a category w/ ajax. '''
        updates = self.process_common(formdata)

        if 'category' in formdata:
            if formdata['category'] == '':
                updates['category'] = None
            else:
                updates['category'] = formdata['category']

        if updates:
            self.actionUpdateSave(**updates)


class Box(ItemModel):
    ''' a flightcase, or other normally-transported-as-one-item thing '''

    home = ForeignKeyField(Location, related_name='boxes', null=True)

    category = ForeignKeyField(Category, related_name='boxes', null=True)

    def process_common(self, formdata):
        ''' this is usually used to update a category w/ ajax. '''
        updates = super(Box, self).process_common(formdata)

        if 'home' in formdata:
            if formdata['home'] == '':
                updates['home'] = None
            else:
                updates['home'] = formdata['home']

        return updates

    # contents -> Content
    # boxes_i_need -> BoxXRef
    # boxes_that_need_me -> BoxXRef
    # cats_i_need -> BoxXCats


class BoxXRef(BaseModel):
    ''' cross-reference table for box related boxes '''
    parent = ForeignKeyField(Box, related_name='boxes_i_need')
    child = ForeignKeyField(Box, related_name='boxes_that_need_me')

    relationship = CharField(default="require")

class BoxXCats(BaseModel):
    ''' cross-reference table for boxes which require 'some' other boxes
        from a category. '''
    parent = ForeignKeyField(Box, related_name='cats_i_need')
    child = ForeignKeyField(Category, related_name='boxes_which_need_these')
    quantity = IntegerField(default=1)

class Content(ItemModel):
    ''' Item within a box.  An amplifier within a flightcase, for instance. '''

    manufacturer = ForeignKeyField(Contact, related_name='makes', null=True)
    supplier = ForeignKeyField(Contact, related_name='supplies', null=True)

    box = ForeignKeyField(Box, related_name='contents', null=True)

    category = ForeignKeyField(Category,
                               related_name='content_items',
                               null=True)

    def process_common(self, formdata):
        ''' update all Content specific fields. Returns dict. '''
        updates = super(Content, self).process_common(formdata)

        if 'box' in formdata:
            if formdata['box'] == '':
                updates['box'] = None
            else:
                updates['box'] = formdata['box']

        return updates


class MultiItem(ItemModel):
    ''' Cables, etc. Things which you have multiple of, and which don't
        require their own bar-code, or whatever. '''

    stock = IntegerField(default=1)

    box = ForeignKeyField(Box, related_name='contained_multis', null=True)

    is_part = BooleanField(default=False)
    part_for = ForeignKeyField(Content, related_name='spare_parts', null=True)

    category = ForeignKeyField(Category, related_name='multi_items', null=True)

    def process_common(self, formdata):
        ''' process all MultiItem fields. return dict '''
        updates = super(MultiItem, self).process_common(formdata)

        if 'stock' in formdata:
            updates['stock'] = formdata['stock']

        if 'box' in formdata:
            if formdata['box'] == '':
                updates['box'] = None
            else:
                updates['box'] = formdata['box']

        return updates


class BoxMultiItemDependency(NormalModel):
    ''' cross-table for many-to-many relationship '''

    box = ForeignKeyField(Box, related_name='multi_items')
    item = ForeignKeyField(MultiItem, related_name='needed_by_boxes')
    quantity = IntegerField(default=1)


################################################################################

class Event(NormalModel):

    start_date = DateField(default=datetime.now())
    end_date = DateField(default=datetime.now())

    status = TextField() # idea, bidding, planning, packing, current, post, past

class EquipmentList(NormalModel):

    event = ForeignKeyField(Event, null=True, related_name='equipmentlists')
    notes = TextField(default='')
    packed = BooleanField(default=False)

    data = TextField(default='[]')

    def process_form(self, formdata):
        self.data = json.dumps(formdata['data'])


class EquipmentListBoxes(NormalModel):

    equipment_list = ForeignKeyField(EquipmentList, related_name='boxes')
    box = ForeignKeyField(Box, related_name='equipment_lists')
    state = IntegerField(default=0)

class EquipmentListMultiItems(NormalModel):

    equipment_list = ForeignKeyField(EquipmentList, related_name='multi_items')
    multi_item = ForeignKeyField(MultiItem, related_name='equipment_lists')

    quantity = IntegerField(default=1)


################################################################################


class Issue(NormalModel):
    box = ForeignKeyField(Box, related_name='issues', null=True)
    item = ForeignKeyField(Content, related_name='issues', null=True)
    multiitem = ForeignKeyField(MultiItem, related_name='issues', null=True)
    event = ForeignKeyField(Event, related_name='issues', null=True)

    resolved = BooleanField(default=False)
    status = TextField(default='Open', null=True)

    opened_date = DateField(default=datetime.now())
    resolved_date = DateField(null=True)

    def resolve(self):
        self.actionUpdateSave(resolved=True, resolved_date=datetime.now())

################################################################################

def create_all(filename):
    ''' initialise the database, create all needed tables '''

    DB.init(filename)

    [t.create_table(True) for t in
        (Location, Category, Box, Content, MultiItem, BoxMultiItemDependency,
         Event, EquipmentList, EquipmentListBoxes, EquipmentListMultiItems,
         UndoAction, BoxXRef, BoxXCats, Contact, Issue
        )]

#####################################################################
