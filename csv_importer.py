import config
import stuff_management.models as m

import csv

# first init. the database:

m.create_all(config.DB)
m.DB.init(config.DB)
m.DB.connect()

# now create the default locations:

for l in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
    column = m.Location()
    column.name = l
    column.uglycode = l
    column.exists = True
    column.save()
    for n in xrange(6):
        loc = m.Location()
        loc.uglycode = l + str(n)
        loc.name = loc.uglycode
        loc.exists = True
        loc.parent = column
        loc.save()

# now actually import the data.

with open('contents.csv', 'rb') as csvfile:
    csvr = csv.reader(csvfile)
    csvr.next()

    for row in csvr:
        #Category,Make,Model Number,Serial No.,Quantity,Box Number,Location,Replacement value,Box Name,Description
        #Box Name,Box Number,Location,Category,Number,Contents
        row = [x.strip() for x in row]
        #cat,make,model,serial,quant,boxid,location,value,boxname,notes = row[:11] 
        boxname, boxid, location, cat, quant, desc = row[:7]
        #        cat, make, model, serial, boxname, notes = row[:7]

        # category:

        try:
            category = m.Category.get(m.Category.name == cat)
        except:
            category = m.Category()
            category.name = cat

        category.exists = True
        category.save()

        # location

        try:
            loc = m.Location.get(m.Location.uglycode == location)

        except Exception as err:
            # print 'L:' + location + ' not found!' + cat + make
            loc = m.Location()
            loc.uglycode = location
            loc.name = location
            loc.exists = True
            loc.save()

        # box:

        if boxid:
            try:
                box = m.Box.get(m.Box.uglycode == boxid)
            except:
                box = m.Box()
                box.uglycode=boxid
                box.name=boxname
                print 'new box!!!' + boxname
        else:
            box = m.Box()
            box.name = '?' + boxname # + make + ' ' + model

        print box.uglycode, boxname
        box.exists=True
        box.category = category
        box.home = loc
        box.save()


        # manufacturer:

        #try:
            #manu = m.Contact.get(m.Content.name == make)
        #except:
            #manu = m.Contact()
            #manu.name = make

        #manu.exists = True
        #manu.save()


        # and the actual item itself:


        if quant == '1' or not quant:
            #if serial:
                #try:
                    #item = m.Content.get((m.Content.serial_number == serial)
                                        #&(m.Content.model_number == model))
                #except:
                    #item = m.Content()
            #else:
            item = m.Content()

        else:
            # multi item...
            print 'multi item ' + item.name
            #if serial:
                #try:
                    #item = m.MultiItem.get(m.MultiItem.serial_number == serial)
                #except:
                    #item = m.MultiItem()
            #else:
            item = m.MultiItem()
            item.stock = int(quant)
            print desc, item.stock

        if item.name == "New":
            if not desc:
                item.name = boxname
            else:
                item.name = desc
            #item.name = model

       # item.model_number = model
        #item.manufacturer = manu
        item.box = box
        item.category = category
        item.description = desc
        item.exists = True
        item.location = loc

        item.save()

        #print box.name, box.uglycode, 'hei'
